/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package abft

import (
	"crypto/md5"
	"encoding/base64"
	"fmt"
	"sync"

	abftpb "chainmaker.org/chainmaker/pb-go/v3/consensus/abft"
	"github.com/thoas/go-funk"
)

// Event transfer msg from acs to abft
type Event struct {
	messages   []*abftpb.ABFTMessageReq // acs.messages, msg should be sent
	rbcOutputs [][]byte                 // acs.rbcOutputs, tx batch should be verified
	outputs    [][]byte                 // acs.outputs, tx batch should be committed
}

type rbcOutput struct {
	id     string
	output []byte
}

// ACS is asynchronous common subset instance
type ACS struct {
	*Config
	sync.Mutex
	rbcInstances map[string]*RBC // key:id, val:*RBC
	bbaInstances map[string]*BBA // key:id, val:*BBA

	messages []*abftpb.ABFTMessageReq // rbc or bba request

	rbcOutputCache map[string]*rbcOutput // key:outputMd5Str, val:outputBz, set after a rbc instance is delivered
	rbcOutputs     [][]byte              // outputBz slice, set after a rbc instance is delivered

	rbcResults map[string][]byte // key:id, val:outputBz, set before input a value to a bba instance
	bbaResults map[string]bool   // key:id, val:0/1, bba result, set after a bba instance is decided

	outputs [][]byte // outputs is acs final output result, calculated based on rbcResults and bbaResults
	decided bool     // decided indicates whether this round consensus is completed
}

// NewACS create acs instance
func NewACS(cfg *Config) *ACS {
	cfg.logger.Infof("NewACS config: %s", cfg)
	acs := &ACS{
		Config:         cfg,
		rbcInstances:   make(map[string]*RBC),
		bbaInstances:   make(map[string]*BBA),
		rbcResults:     make(map[string][]byte),
		bbaResults:     make(map[string]bool),
		rbcOutputCache: make(map[string]*rbcOutput),
	}

	for _, id := range cfg.nodes {
		rbcCfg := cfg.clone()
		rbcCfg.id = id
		acs.rbcInstances[id] = NewRBC(rbcCfg)

		bbaCfg := cfg.clone()
		bbaCfg.id = id
		acs.bbaInstances[id] = NewBBA(bbaCfg)
	}
	return acs
}

// InputRBC process tx batch
func (acs *ACS) InputRBC(val []byte) error {
	acs.Lock()
	defer acs.Unlock()
	acs.logger.Infof("[%s](%d) ACS inputRBC id: %v", acs.nodeID, acs.height, acs.id)
	for _, id := range acs.nodes {
		if id == acs.nodeID {
			rbc, ok := acs.rbcInstances[acs.nodeID]
			if !ok {
				return fmt.Errorf("[%s](%d) cannot find rbc instance: %s", acs.nodeID, acs.height, acs.nodeID)
			}

			if err := rbc.Input(val); err != nil {
				return err
			}

			acs.appendMessages(rbc.Messages())

			if output := rbc.Output(); output != nil {
				acs.handleRBCOutput(acs.nodeID, output)
			}
		}
	}

	return nil
}

// InputBBA process the verify result of tx batch
func (acs *ACS) InputBBA(output []byte) error {
	acs.Lock()
	defer acs.Unlock()

	hash := md5.Sum(output)
	hashStr := base64.StdEncoding.EncodeToString(hash[:])
	rbcOutput, ok := acs.rbcOutputCache[hashStr]
	if !ok {
		return fmt.Errorf("[%s](%d-%s) ACS receive invalid BBA: %v, this should not happen",
			acs.nodeID, acs.height, acs.nodeID, hashStr)
	}

	// add rbc output to rbcResults before input bba
	acs.rbcResults[rbcOutput.id] = rbcOutput.output
	acs.logger.Infof("[%s](%d) ACS InputBBA id: %v", acs.nodeID, acs.height, rbcOutput.id)

	// may be bba instance is finished and only wait this rbc to complete
	// in this case, we can finish acs now
	if acs.tryComplete() {
		return nil
	}

	// acs is not finished, acs input 1 to this bba instance
	return acs.processBBA(rbcOutput.id, func(bba *BBA) error {
		if bba.AcceptInput() {
			return bba.Input(true)
		}
		return nil
	})
}

// HandleMessage handle rbc or bba message
func (acs *ACS) HandleMessage(sender string, id string, acsMessage *abftpb.ACSMessage) error {
	acs.Lock()
	defer acs.Unlock()

	switch m := acsMessage.Message.(type) {
	case *abftpb.ACSMessage_Rbc:
		return acs.handleRBC(sender, id, m.Rbc)
	case *abftpb.ACSMessage_Bba:
		return acs.handleBBA(sender, id, m.Bba)
	default:
		acs.logger.Errorf("[%s](%d-%s) ACS receive invalid message: %+v, this should not happen",
			acs.nodeID, acs.height, id, m)
	}
	return nil
}

// Event deliver event to ABFT
func (acs *ACS) Event() *Event {
	acs.Lock()
	defer acs.Unlock()

	event := &Event{}
	if len(acs.rbcOutputs) != 0 {
		event.rbcOutputs = acs.rbcOutputs
		acs.rbcOutputs = nil
	}
	if len(acs.messages) != 0 {
		event.messages = acs.messages
		acs.messages = nil
	}
	if len(acs.outputs) != 0 {
		event.outputs = acs.outputs
		acs.outputs = nil
	}
	return event
}

func (acs *ACS) handleRBC(sender string, id string, rbcMessage *abftpb.RBCRequest) error {
	return acs.processRBC(id, func(rbc *RBC) error {
		return rbc.HandleMessage(sender, rbcMessage)
	})
}

func (acs *ACS) handleBBA(sender string, id string, bbaMessage *abftpb.BBARequest) error {
	return acs.processBBA(id, func(bba *BBA) error {
		return bba.HandleMessage(sender, bbaMessage)
	})
}

func (acs *ACS) appendMessages(msgs []*abftpb.ABFTMessageReq) {
	acs.messages = append(acs.messages, msgs...)
}

func (acs *ACS) processRBC(id string, f func(rbc *RBC) error) error {
	rbc, ok := acs.rbcInstances[id]
	if !ok {
		return fmt.Errorf("[%s](%d) cannot find RBC instance: %s", acs.nodeID, acs.height, id)
	}

	if err := f(rbc); err != nil {
		return err
	}
	acs.logger.Infof("[%s](%d) ACS processRBC id: %v", acs.nodeID, acs.height, id)

	acs.appendMessages(rbc.Messages())

	if output := rbc.Output(); output != nil {
		acs.handleRBCOutput(id, output)
	}
	return nil
}

func (acs *ACS) handleRBCOutput(id string, output []byte) {
	data := &rbcOutput{
		id:     id,
		output: output,
	}
	hash := md5.Sum(output)
	hashStr := base64.StdEncoding.EncodeToString(hash[:])
	acs.rbcOutputCache[hashStr] = data
	acs.rbcOutputs = append(acs.rbcOutputs, output)
}

func (acs *ACS) processBBA(id string, f func(bba *BBA) error) error {
	bba, ok := acs.bbaInstances[id]
	if !ok {
		return fmt.Errorf("[%s](%d) cannot find ABA instance: %s", acs.nodeID, acs.height, id)
	}
	// if bba instance is finished
	// bba msg is no longer processed
	if bba.done {
		return nil
	}
	// process msg in bba instance
	if err := f(bba); err != nil {
		return err
	}
	acs.logger.Infof("[%s](%d) ACS processBBA id: %v", acs.nodeID, acs.height, id)

	acs.appendMessages(bba.Messages())

	// if bba instance is decided for the first time,
	// add result 0/1 to bbaResults, judge whether input false to other bba instances, and try complete acs.
	if outputted, output := bba.Output(); outputted {
		if _, ok = acs.bbaResults[id]; ok {
			acs.logger.Errorf("[%s](%d-%s) BBA outputs multiple result: %v", acs.nodeID, acs.height, id, acs.bbaResults[id])
			return fmt.Errorf("[%s](%d-%s) BBA outputs multiple result: %v", acs.nodeID, acs.height, id, acs.bbaResults[id])
		}

		// add result 0/1 to bbaResults
		acs.bbaResults[id] = output

		// upon delivery of value 1 from at least N-f instances of bba,
		// provide input 0 to each instance of BA that has not yet been provided input.
		if output && acs.countFinishedBBA() == acs.nodesNum-acs.faultsNum {
			for idIns, bbaIns := range acs.bbaInstances {
				if bbaIns.AcceptInput() {
					if err := bbaIns.Input(false); err != nil {
						return err
					}

					acs.appendMessages(bbaIns.Messages())
					if outputtedIns, outputIns := bbaIns.Output(); outputtedIns {
						acs.bbaResults[idIns] = outputIns
					}
				}
			}
		}

		// judge whether acs can be finished
		// the finished condition is that all bba finished and at least N-f bba instances is 1
		acs.tryComplete()
	}
	return nil
}

// countFinishedBBA count finished BBA with true
func (acs *ACS) countFinishedBBA() int {
	n := 0
	for _, ok := range acs.bbaResults {
		if ok {
			n++
		}
	}
	return n
}

func (acs *ACS) tryComplete() bool {
	acs.logger.Debugf("[%s](%d) ACS tryComplete decided: %v, finishedBBACount: %v, bbaResults.len: %v",
		acs.nodeID, acs.height, acs.decided, acs.countFinishedBBA(), len(acs.bbaResults))
	// judge whether all bba has outputted/decided and N-f bba instance decided with 1
	if acs.decided ||
		acs.countFinishedBBA() < acs.nodesNum-acs.faultsNum ||
		len(acs.bbaResults) < acs.nodesNum {
		return false
	}

	// finish acs for the first time
	// acs finish condition is that:
	// (1) all bba instances have outputted/decided
	// (2) at least N-f bba instances decide with 1 (that's because it takes at least N-f RBC instances to delivered)
	// (3) all rbc instances with bba decided with 1 have delivered

	bbaTrueDecision := []string{}
	for id, ok := range acs.bbaResults {
		if ok {
			bbaTrueDecision = append(bbaTrueDecision, id)
		}
	}

	// if there is rbc is not delivered,
	// return and wait for rbc to complete
	outputs := make([][]byte, 0)
	for _, id := range bbaTrueDecision {
		val, ok := acs.rbcResults[id]
		if !ok {
			acs.logger.Infof("[%s](%d) ACS tryComplete wait for rbcResults id: %v, rbcResults: %v",
				acs.nodeID, acs.height, id, funk.Keys(acs.rbcResults))
			return false
		}
		outputs = append(outputs, val)
	}

	// finish acs that all bba instances have decided and some rbc(that bba decided with 1) have delivered
	acs.outputs = outputs
	acs.decided = true
	acs.logger.Infof("[%s](%d) ACS complete output.len: %v, output: %v",
		acs.nodeID, acs.height, len(acs.outputs), bbaTrueDecision)
	return true
}

// ACSStateJson is acs state json
type ACSStateJson struct {
	RBCStates map[string]*RBCStateJson `json:"rbc_states"`
	BBAStates map[string]*BBAStateJson `json:"bba_states"`

	RBCResults map[string][]byte `json:"rbc_results"`
	BBAResults map[string]bool   `json:"bba_results"`

	Outputs [][]byte `json:"outputs"`
	Decided bool     `json:"decided"`
}

// GetACSStateJson return ACSStateJson
func (acs *ACS) GetACSStateJson() *ACSStateJson {
	acs.Lock()
	defer acs.Unlock()

	rbcStates := make(map[string]*RBCStateJson)
	for id, rbc := range acs.rbcInstances {
		rbcStates[id] = rbc.GetRBCStateJson()
	}

	bbaStates := make(map[string]*BBAStateJson)
	for id, bba := range acs.bbaInstances {
		bbaStates[id] = bba.GetBBAStateJson()
	}

	return &ACSStateJson{
		RBCStates: rbcStates,
		BBAStates: bbaStates,

		RBCResults: acs.rbcResults,
		BBAResults: acs.bbaResults,

		Outputs: acs.outputs,
		Decided: acs.decided,
	}
}
