/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package abft

import (
	"os/exec"
	"testing"
	"time"

	consensusutils "chainmaker.org/chainmaker/consensus-utils/v3"
	"chainmaker.org/chainmaker/consensus-utils/v3/testframework"
	"chainmaker.org/chainmaker/logger/v3"
	configpb "chainmaker.org/chainmaker/pb-go/v3/config"
	consensuspb "chainmaker.org/chainmaker/pb-go/v3/consensus"
	"chainmaker.org/chainmaker/protocol/v3"
	"github.com/golang/mock/gomock"
	"github.com/test-go/testify/require"
)

var (
	chainId          = "chain1"
	nodeNums         = 4
	consensusType    = consensuspb.ConsensusType_ABFT
	ConsensusEngines = make([]protocol.ConsensusEngine, nodeNums)
	CoreEngines      = make([]protocol.CoreEngine, nodeNums)
)

func TestConsensus_ABFT(t *testing.T) {
	cmd := exec.Command("/bin/sh", "-c", "rm -rf chain1 default.log*")
	err := cmd.Run()
	require.Nil(t, err)

	err = testframework.InitLocalConfigs()
	require.Nil(t, err)
	defer testframework.RemoveLocalConfigs()

	testframework.SetTxSizeAndTxNum(200, 10*1024)

	// init LocalConfig
	testframework.InitLocalConfig(nodeNums)
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	//create test_node_configs
	testNodeConfigs, err := testframework.CreateTestNodeConfig(ctrl, chainId, consensusType, nodeNums,
		nil, nil, func(cfg *configpb.ChainConfig) []byte { return nil })
	if err != nil {
		t.Errorf("%v", err)
	}

	tLogger := logger.GetLogger(chainId)
	for i := 0; i < nodeNums; i++ {
		// new CoreEngine
		CoreEngines[i] = testframework.NewCoreEngineForTest(testNodeConfigs[i], tLogger)
	}
	for i := 0; i < nodeNums; i++ {
		netService := testframework.NewNetServiceForTest()
		tc := &consensusutils.ConsensusImplConfig{
			ChainId:     testNodeConfigs[i].ChainID,
			NodeId:      testNodeConfigs[i].NodeId,
			Ac:          testNodeConfigs[i].Ac,
			ChainConf:   testNodeConfigs[i].ChainConf,
			NetService:  netService,
			Core:        CoreEngines[i],
			Signer:      testNodeConfigs[i].Signer,
			LedgerCache: testNodeConfigs[i].LedgerCache,
			MsgBus:      testNodeConfigs[i].MsgBus,
			Logger:      tLogger,
		}

		consensus, errs := New(tc)
		if errs != nil {
			t.Errorf("%v", errs)
		}

		ConsensusEngines[i] = consensus
	}

	tf, err := testframework.NewTestClusterFramework(chainId, consensusType, nodeNums, testNodeConfigs, ConsensusEngines, CoreEngines)
	require.Nil(t, err)

	// set log
	l := &logger.LogConfig{
		SystemLog: logger.LogNodeConfig{
			FilePath:        "./default.log",
			LogLevelDefault: "INFO",
			LogLevels:       map[string]string{"consensus": "DEBUG", "core": "INFO", "net": "INFO"},
			LogInConsole:    false,
			ShowColor:       true,
		},
	}
	logger.SetLogConfig(l)

	tf.Start()
	time.Sleep(60 * time.Second)
	tf.Stop()

	cmd = exec.Command("/bin/sh", "-c", "cat default.log|grep TPS")
	out, err := cmd.CombinedOutput()
	require.Nil(t, err)
	require.NotNil(t, out)
}
