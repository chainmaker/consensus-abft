/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package abft

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"path"
	"sync"

	"chainmaker.org/chainmaker/common/v3/msgbus"
	consensusutils "chainmaker.org/chainmaker/consensus-utils/v3"
	"chainmaker.org/chainmaker/localconf/v3"
	"chainmaker.org/chainmaker/lws"
	"chainmaker.org/chainmaker/pb-go/v3/common"
	"chainmaker.org/chainmaker/pb-go/v3/config"
	consensuspb "chainmaker.org/chainmaker/pb-go/v3/consensus"
	abftpb "chainmaker.org/chainmaker/pb-go/v3/consensus/abft"
	netpb "chainmaker.org/chainmaker/pb-go/v3/net"
	"chainmaker.org/chainmaker/protocol/v3"
	"chainmaker.org/chainmaker/utils/v3"
	"github.com/gogo/protobuf/proto"
	"github.com/thoas/go-funk"
)

var (
	_ protocol.ConsensusEngine = (*ConsensusABFTImpl)(nil)
	// wal file name
	walDir = "abftwal_%s"
)

// mustMarshal marshals protobuf message to byte slice or panic
func mustMarshal(msg proto.Message) []byte {
	data, err := proto.Marshal(msg)
	if err != nil {
		panic(err)
	}
	return data
}

// mustUnmarshal unmarshals from byte slice to protobuf message or panic
func mustUnmarshal(b []byte, msg proto.Message) {
	if err := proto.Unmarshal(b, msg); err != nil {
		panic(err)
	}
}

// ConsensusABFTImpl is the implementation of ABFT algorithm
// and it implements the ConsensusEngine interface.
type ConsensusABFTImpl struct {
	sync.RWMutex
	chainID   string                   // chainId
	Id        string                   // nodeId
	height    uint64                   // current consensus height
	acs       *ACS                     // acs instance contains N rbc and bba instance
	msgSender *msgSender               // resending msg to ensures that the message can be sent to other nodes
	msgBuffer []*abftpb.ABFTMessageReq // cache consensus msg in higher consensus

	wal              *lws.Lws // wal to persistent consensus state
	waldir           string   // wal save path
	heightFirstIndex uint64   // the first index to save consensus state in current height consensus
	lastIndex        uint64   // the last index for consensus state to write wal

	closeC chan struct{} // close resending msg service

	msgbus      msgbus.MessageBus
	chainConf   protocol.ChainConf
	singer      protocol.SigningMember
	ledgerCache protocol.LedgerCache
	logger      protocol.Logger
}

// New creates a abft consensus instance
func New(config *consensusutils.ConsensusImplConfig) (*ConsensusABFTImpl, error) {
	config.Logger.Infof("New ConsensusABFTImpl[%s]", config.NodeId)
	consensus := &ConsensusABFTImpl{}
	consensus.logger = config.Logger
	consensus.chainID = config.ChainId
	consensus.Id = config.NodeId
	consensus.msgbus = config.MsgBus
	consensus.chainConf = config.ChainConf
	consensus.singer = config.Signer
	consensus.ledgerCache = config.LedgerCache
	consensus.msgBuffer = make([]*abftpb.ABFTMessageReq, 0)
	consensus.msgSender = newMsgSender(consensus.logger, consensus.Id)
	consensus.closeC = make(chan struct{})

	height, err := config.LedgerCache.CurrentHeight()
	if err != nil {
		return nil, err
	}
	consensus.height = height + 1

	storePath := localconf.ChainMakerConfig.GetStorePath()
	consensus.waldir = path.Join(storePath, consensus.chainID, fmt.Sprintf(walDir, consensus.Id))
	consensus.wal, err = lws.Open(consensus.waldir, lws.WithWriteFlag(lws.WF_SYNCFLUSH, 0))
	if err != nil {
		return nil, err
	}

	return consensus, nil
}

// Start implements the Stop method of ConsensusEngine interface
// and starts the abft instance.
func (consensus *ConsensusABFTImpl) Start() error {
	nodeList := GetNodeListFromConfig(consensus.chainConf.ChainConfig())

	for _, id := range nodeList {
		if id == consensus.Id {
			cfg := &Config{
				logger: consensus.logger,
				height: consensus.height,
				id:     consensus.Id,
				nodeID: consensus.Id,
				nodes:  nodeList,
			}
			cfg.fillWithDefaults()
			consensus.acs = NewACS(cfg)
			go consensus.run()
			err := consensus.replayWal()
			if err != nil {
				return err
			}

			consensus.sendPackageSingal(consensus.height)
			consensus.msgbus.Register(msgbus.ProposedBlock, consensus)
			consensus.msgbus.Register(msgbus.VerifyResult, consensus)
			consensus.msgbus.Register(msgbus.BlockInfo, consensus)
			consensus.msgbus.Register(msgbus.RecvConsensusMsg, consensus)

			consensus.logger.Infof("[%s](%d) ABFT consensus started", consensus.Id, consensus.height)
		}
	}
	return nil
}

// Stop implements the Stop method of ConsensusEngine interface
// and stops the abft instance.
func (consensus *ConsensusABFTImpl) Stop() error {
	consensus.Lock()
	defer consensus.Unlock()

	close(consensus.closeC)
	consensus.logger.Infof("[%s](%d) ABFT consensus stopped", consensus.Id, consensus.height)
	return nil
}

// GetAllNodeInfos Get consensus nodes info
func (consensus *ConsensusABFTImpl) GetAllNodeInfos() []protocol.ConsensusNodeInfo {
	var consensusNodeInfo []protocol.ConsensusNodeInfo
	for _, orgConf := range consensus.chainConf.ChainConfig().Consensus.Nodes {
		for _, nid := range orgConf.NodeId {
			consensusNodeInfo = append(consensusNodeInfo, protocol.ConsensusNodeInfo{
				NodeID: nid,
				State:  protocol.ACTIVE,
			})
		}
	}
	return consensusNodeInfo
}

// GetValidators get validators
func (consensus *ConsensusABFTImpl) GetValidators() ([]string, error) {
	var nodeIds []string
	for _, orgConf := range consensus.chainConf.ChainConfig().Consensus.Nodes {
		nodeIds = append(nodeIds, orgConf.NodeId...)
	}
	return nodeIds, nil
}

// GetConsensusStateJSON get consensus state json
func (consensus *ConsensusABFTImpl) GetConsensusStateJSON() ([]byte, error) {
	consensus.RLock()
	defer consensus.RUnlock()

	acsState := consensus.acs.GetACSStateJson()
	return json.Marshal(acsState)
}

// GetConsensusType get consensus typs
func (consensus *ConsensusABFTImpl) GetConsensusType() consensuspb.ConsensusType {
	return consensuspb.ConsensusType_ABFT
}

// GetLastHeight get last height
func (consensus *ConsensusABFTImpl) GetLastHeight() uint64 {
	consensus.RLock()
	defer consensus.RUnlock()

	return consensus.height
}

// OnMessage implements the OnMessage interface of msgbus.Subscriber
func (consensus *ConsensusABFTImpl) OnMessage(message *msgbus.Message) {
	consensus.onMessage(message, false)
}

func (consensus *ConsensusABFTImpl) onMessage(message *msgbus.Message, replayWalMode bool) {
	consensus.Lock()
	defer consensus.Unlock()

	consensus.logger.Debugf("[%s](%v) OnMessage receive topic: %s", consensus.Id, consensus.height, message.Topic)

	if !replayWalMode {
		consensus.saveWalEntry(message)
	}

	switch message.Topic {
	case msgbus.ProposedBlock:
		consensus.onProposedBlock(message)
	case msgbus.VerifyResult:
		consensus.onVerifyResult(message)
	case msgbus.BlockInfo:
		consensus.onBlockInfo(message)
	case msgbus.RecvConsensusMsg:
		consensus.onRecvConsensusMsg(message)
	}
}

func (consensus *ConsensusABFTImpl) run() {
	for {
		select {
		case <-consensus.closeC:
			return
		case msg := <-consensus.msgSender.msgCh:
			consensus.logger.Debugf("[%s](%d) send req seq: %v, height: %v, from: %v, Id: %v",
				consensus.Id, consensus.height, msg.Seq, msg.Height, msg.From, msg.To)
			abftMessage := &abftpb.ABFTMessage{
				Message: &abftpb.ABFTMessage_Req{Req: msg},
			}
			netMsg := &netpb.NetMsg{
				Payload: mustMarshal(abftMessage),
				Type:    netpb.NetMsg_CONSENSUS_MSG,
				To:      msg.To,
			}
			consensus.publishToMsgbus(netMsg)
		}
	}
}

// OnQuit implements the OnQuit interface of msgbus.Subscriber
func (consensus *ConsensusABFTImpl) OnQuit() {
	consensus.logger.Debugf("[%s] OnQuit", consensus.Id)
}

func (consensus *ConsensusABFTImpl) onProposedBlock(message *msgbus.Message) {
	block, ok := message.Payload.(*common.Block)
	if !ok {
		consensus.logger.Errorf("[%s](%v) receive wrong payload which should be Block", consensus.Id, consensus.height)
		return
	}
	if block == nil || block.Header == nil {
		consensus.logger.Errorf("receive message failed, error message Block = nil")
		return
	}

	if block.Header.BlockHeight != consensus.height {
		consensus.logger.Warnf("[%s](%v) receive wrong proposed block height: %v",
			consensus.Id, consensus.height, block.Header.BlockHeight)
		return
	}

	// Add hash and signature to block
	hash, sig, err := utils.SignBlock(consensus.chainConf.ChainConfig().Crypto.Hash, consensus.singer, block)
	if err != nil {
		consensus.logger.Errorf("[%s]sign block failed, %s", consensus.Id, err)
		return
	}
	block.Header.BlockHash = hash[:]
	block.Header.Signature = sig

	consensus.logger.Infof("[%s](%v) receive proposed block: (%v-%x-%x)",
		consensus.Id, consensus.height, block.Header.BlockHeight, block.Header.BlockHash, block.Header.PreBlockHash)

	// begin rbc instance to reliable broadcast block
	data := mustMarshal(block)
	if err = consensus.acs.InputRBC(data); err != nil {
		consensus.logger.Errorf("[%s] input RBC error: %v", consensus.Id, err)
	}
	// process event
	consensus.processEvent()
}

func (consensus *ConsensusABFTImpl) onVerifyResult(message *msgbus.Message) {
	verifyResult, ok := message.Payload.(*consensuspb.VerifyResult)
	if !ok {
		consensus.logger.Errorf("[%s](%v) receive wrong payload which should be VerifiedBlock",
			consensus.Id, consensus.height)
		return
	}
	if verifyResult == nil || verifyResult.VerifiedBlock == nil || verifyResult.VerifiedBlock.Header == nil {
		consensus.logger.Errorf("receive message failed, error message VerifyResult = nil")
		return
	}

	if verifyResult.VerifiedBlock.Header.BlockHeight != consensus.height {
		consensus.logger.Warnf("[%s](%v) receive wrong verifyResult height: %v",
			consensus.Id, consensus.height, verifyResult.VerifiedBlock.Header.BlockHeight)
		return
	}

	consensus.logger.Infof("[%s](%v) verify block result: %s, msg: %s, block: (%v-%x-%x)",
		consensus.Id, consensus.height, verifyResult.Code, verifyResult.Msg, verifyResult.VerifiedBlock.Header.BlockHeight,
		verifyResult.VerifiedBlock.Header.BlockHash, verifyResult.VerifiedBlock.Header.PreBlockHash)

	if verifyResult.Code != consensuspb.VerifyResult_SUCCESS {
		return
	}

	// input valid tx batch to BBA
	data := mustMarshal(verifyResult.VerifiedBlock)
	err := consensus.acs.InputBBA(data)
	if err != nil {
		consensus.logger.Errorf("acs input error: %v", err)
	}

	// process event
	consensus.processEvent()
}

func (consensus *ConsensusABFTImpl) onBlockInfo(message *msgbus.Message) {
	blockInfo, ok := message.Payload.(*common.BlockInfo)
	if !ok {
		consensus.logger.Errorf("[%s](%v) receive wrong payload which should be BlockInfo", consensus.Id, consensus.height)
		return
	}
	if blockInfo == nil || blockInfo.Block == nil || blockInfo.Block.Header == nil {
		consensus.logger.Errorf("receive message failed, error message BlockInfo = nil")
		return
	}

	if blockInfo.Block.Header.BlockHeight < consensus.height {
		consensus.logger.Warnf("[%s](%v) receive overdue block info: %v",
			consensus.Id, consensus.height, blockInfo.Block.Header.BlockHeight)
		return
	}

	consensus.logger.Infof("[%s](%v) receive block info: %v",
		consensus.Id, consensus.height, blockInfo.Block.Header.BlockHeight)

	// update heightFirstIndex
	consensus.heightFirstIndex = consensus.lastIndex + 1

	// gc wal
	err := consensus.deleteWalEntry(consensus.height)
	if err != nil {
		consensus.logger.Fatalf("[%s] failed to delete wal log %v",
			consensus.Id, consensus.height, err)
	}

	// close msgSender in current height
	consensus.height = blockInfo.Block.Header.BlockHeight + 1
	nodeList := GetNodeListFromConfig(consensus.chainConf.ChainConfig())

	for _, id := range nodeList {
		if id == consensus.Id {
			consensus.msgSender.cleanHeight(consensus.height - 1)
		}
	}

	// enter new height consensus
	cfg := &Config{
		logger: consensus.logger,
		height: consensus.height,
		id:     consensus.Id,
		nodeID: consensus.Id,
		nodes:  nodeList,
	}
	cfg.fillWithDefaults()
	consensus.acs = NewACS(cfg)

	// notify core to propose a block
	consensus.sendPackageSingal(consensus.height)

	// process message in new height
	buffer := make([]*abftpb.ABFTMessageReq, 0)
	for _, msg := range consensus.msgBuffer {
		if msg.Height == consensus.height {
			consensus.handleMessage(msg)
		} else if msg.Height > consensus.height {
			buffer = append(buffer, msg)
		}
	}
	consensus.msgBuffer = buffer

	// process event
	consensus.processEvent()
}

func (consensus *ConsensusABFTImpl) onRecvConsensusMsg(message *msgbus.Message) {
	msg, ok := message.Payload.(*netpb.NetMsg)
	if !ok {
		consensus.logger.Errorf("[%s](%v) receive wrong payload which should be NetMsg", consensus.Id, consensus.height)
		return
	}

	abftMsg := new(abftpb.ABFTMessage)
	mustUnmarshal(msg.Payload, abftMsg)

	switch m := abftMsg.Message.(type) {
	case *abftpb.ABFTMessage_Req:
		consensus.handleMessage(m.Req)
		consensus.processEvent()
	case *abftpb.ABFTMessage_Rsp:
		consensus.msgSender.ack(m.Rsp)
	}
}

func (consensus *ConsensusABFTImpl) processEvent() {
	event := consensus.acs.Event()
	if event == nil {
		return
	}

	// notify core to verify tx batch that rbc instance has delivered
	if event.rbcOutputs != nil {
		for _, output := range event.rbcOutputs {
			block := &common.Block{}
			mustUnmarshal(output, block)
			consensus.msgbus.PublishSafe(msgbus.VerifyBlock, block)
			consensus.logger.Infof("[%s](%v) verify block: (%v-%x-%x)",
				consensus.Id, consensus.height, block.Header.BlockHeight, block.Header.BlockHash, block.Header.PreBlockHash)
		}
	}

	// process rbc or bba request
	if event.messages != nil {
		for _, msg := range event.messages {
			consensus.handleMessage(msg)
		}
	}

	// notify core to commit tx batches that acs has completed
	if event.outputs != nil && len(event.outputs) != 0 {
		var txBatchHash [][]byte
		for _, data := range event.outputs {
			block := &common.Block{}
			mustUnmarshal(data, block)
			txBatchHash = append(txBatchHash, block.Header.BlockHash)
		}
		consensus.msgbus.PublishSafe(msgbus.CommitedTxBatchs, &abftpb.TxBatchAfterABA{
			BlockHeight: consensus.height,
			TxBatchHash: txBatchHash,
		})
		consensus.logger.Infof("[%s](%v) commit batches: %v",
			consensus.Id, consensus.height,
			funk.Map(txBatchHash, func(data []byte) string { return hex.EncodeToString(data) }))
	}
}

// handleMessage process RBC or BBA message
func (consensus *ConsensusABFTImpl) handleMessage(msg *abftpb.ABFTMessageReq) {
	consensus.logger.Debugf("[%s](%d) handleMessage seq: %v, height: %v, from: %v, to: %v, Id: %v",
		consensus.Id, consensus.height, msg.Seq, msg.Height, msg.From, msg.To, msg.Id)

	if msg.Height < consensus.height {
		// response with outdated error
		consensus.responseWithCode(msg, abftpb.ErrorCode_FailOfOutdatedHeight)
		return
	} else if msg.Height > consensus.height {
		// if the node is backward, block can be synchronized using sync module
		// then abft will enter new height and continue to consensus
		//consensus.msgBuffer = append(consensus.msgBuffer, msg)
		return
	}

	// process msg in this height
	// send msg to other nodes
	if msg.To != consensus.Id {
		// Send messages to other nodes
		consensus.msgSender.addMsg(msg)
		return
	}

	// process msg sent to self
	err := consensus.acs.HandleMessage(msg.From, msg.Id, msg.Acs)
	if err != nil {
		if err == ErrDuplicatedRBCRequest {
			// notice process result for other nodes
			if msg.From != consensus.Id {
				consensus.responseWithCode(msg, abftpb.ErrorCode_Success)
			}
		} else {
			consensus.logger.Errorf("[%s] handleMessage to: %s, error: %v", consensus.Id, msg.To, err)
			// notice process result for other nodes
			if msg.From != consensus.Id {
				consensus.responseWithCode(msg, abftpb.ErrorCode_FailOfUnkown)
			}
		}
		return
	}
	// notice process result for other nodes
	if msg.From != consensus.Id {
		consensus.responseWithCode(msg, abftpb.ErrorCode_Success)
	}
}

func (consensus *ConsensusABFTImpl) sendPackageSingal(height uint64) {
	consensus.logger.Infof("[%s] sendPackageSingal height: %d", consensus.Id, height)
	signal := &abftpb.PackagedSignal{BlockHeight: height}
	consensus.msgbus.PublishSafe(msgbus.PackageSignal, signal)
}

func (consensus *ConsensusABFTImpl) publishToMsgbus(msg *netpb.NetMsg) {
	consensus.msgbus.PublishSafe(msgbus.SendConsensusMsg, msg)
}

func (consensus *ConsensusABFTImpl) responseWithCode(msg *abftpb.ABFTMessageReq, code abftpb.ErrorCode) {
	consensus.logger.Debugf("[%s](%d) responseWithCode msg seq: %v, height: %v, from: %v, Id: %v, code: %s",
		consensus.Id, consensus.height, msg.Seq, msg.Height, msg.From, msg.Id, code)
	abftMessage := &abftpb.ABFTMessage{
		Message: &abftpb.ABFTMessage_Rsp{
			Rsp: &abftpb.ABFTMessageRsp{
				Seq:    msg.Seq,
				Height: msg.Height,
				From:   consensus.Id,
				To:     msg.From,
				Id:     msg.Id,
				Code:   code,
			},
		},
	}
	netMsg := &netpb.NetMsg{
		Payload: mustMarshal(abftMessage),
		Type:    netpb.NetMsg_CONSENSUS_MSG,
		To:      msg.From,
	}
	consensus.publishToMsgbus(netMsg)
}

// GetNodeListFromConfig get consensus node list
func GetNodeListFromConfig(chainConfig *config.ChainConfig) (validators []string) {
	nodes := chainConfig.Consensus.Nodes
	for _, node := range nodes {
		validators = append(validators, node.NodeId...)
	}
	return validators
}

// VerifyBlockSignatures verifies whether the signatures in block
// is qualified with the consensus algorithm. It should return nil
// error when verify successfully, and return corresponding error
// when failed.
func VerifyBlockSignatures(chainConf protocol.ChainConf, ac protocol.AccessControlProvider, block *common.Block) error {
	return nil
}

// saveWalEntry saves entry to Wal
func (consensus *ConsensusABFTImpl) saveWalEntry(message *msgbus.Message) {
	var data []byte
	switch message.Topic {
	case msgbus.ProposedBlock:
		data = mustMarshal(message.Payload.(*common.Block))
	case msgbus.VerifyResult:
		data = mustMarshal(message.Payload.(*consensuspb.VerifyResult))
	case msgbus.BlockInfo:
		data = mustMarshal(message.Payload.(*common.BlockInfo))
	case msgbus.RecvConsensusMsg:
		data = mustMarshal(message.Payload.(*netpb.NetMsg))
	default:
		consensus.logger.Fatalf("[%s](%d) save wal of unknown topic: %s",
			consensus.Id, consensus.height, message.Topic)
	}

	walEntry := &abftpb.WalEntry{
		Height:           consensus.height,
		HeightFirstIndex: consensus.heightFirstIndex,
		Topic:            int32(message.Topic),
		Data:             data,
	}

	// write wal and update lastIndex
	lastIndex, err := consensus.wal.WriteRetIndex(0, mustMarshal(walEntry))
	if err != nil {
		consensus.logger.Fatalf("[%s](%d) saveWalEntry topic: %s write failed, err: %v",
			consensus.Id, consensus.height, message.Topic, err)
	}
	consensus.lastIndex = lastIndex

	consensus.logger.Debugf("[%s](%d) saveWalEntry, topic: %s, index:%d, data length: %v",
		consensus.Id, consensus.height, message.Topic, lastIndex, len(data))
}

// replayWal replays the wal when the node starting
func (consensus *ConsensusABFTImpl) replayWal() error {
	currentHeight, err := consensus.ledgerCache.CurrentHeight()
	if err != nil {
		return err
	}
	consensus.logger.Infof("[%s] replayWal currentHeight: %d", consensus.Id, currentHeight)

	// initial an iterator for wal file and skip to the end of the file
	iter := consensus.wal.NewLogIterator()
	defer iter.Release()

	iter.SkipToLast()
	if !iter.HasPre() {
		// set lastIndex and heightFirstIndex for first start
		consensus.lastIndex = iter.Previous().Index()
		consensus.heightFirstIndex = consensus.lastIndex + 1
		consensus.logger.Infof("[%s] replayWal can't found log entry in wal, lastIndex: %d, heightFirstIndex: %d",
			consensus.Id, consensus.lastIndex, consensus.heightFirstIndex)
		return nil
	}

	lastElement := iter.Previous()
	lastData, err := lastElement.Get()
	if err != nil {
		consensus.logger.Errorf("[%s] replayWal last log entry failed, err: %v", consensus.Id, err)
		return err
	}
	lastEntry := &abftpb.WalEntry{}
	mustUnmarshal(lastData, lastEntry)

	lastIndex := lastElement.Index()
	lastHeight := lastEntry.Height
	if currentHeight < lastHeight-1 {
		consensus.logger.Fatalf("[%s] replay currentHeight: %v < lastHeight-1: %v, this should not happen",
			consensus.Id, currentHeight, lastHeight-1)
	}

	// set lastIndex and heightFirstIndex for restart
	consensus.lastIndex = lastIndex
	consensus.heightFirstIndex = consensus.lastIndex + 1

	// consensus is slower than ledger should not replay wal
	if currentHeight >= lastHeight {
		consensus.logger.Infof("[%s] replayWal should not replay log entry in wal,"+
			"currentHeight: %d, walHeight: %d, lastIndex: %d, heightFirstIndex: %d",
			consensus.Id, currentHeight, lastHeight, consensus.lastIndex, consensus.heightFirstIndex)
		return nil
	}

	// consensus is higher than ledger replay wal log
	// skip to the first element to replay
	entryCount := lastIndex - lastEntry.HeightFirstIndex + 1
	if entryCount <= 0 {
		consensus.logger.Fatalf("[%s] replayWal replay lastIndex: %v < HeightFirstIndex: %v, this should not happen",
			consensus.Id, lastIndex, lastEntry.HeightFirstIndex)
	}
	iter.PreviousN(int(entryCount))

	for iter.HasNext() {
		var (
			element *lws.EntryElemnet
			data    []byte
		)
		element = iter.Next()
		data, err = element.Get()
		if err != nil {
			consensus.logger.Errorf("[%s] replayWal replay log entry failed, index: %d, err: %v",
				consensus.Id, element.Index(), err)
			return err
		}
		entry := &abftpb.WalEntry{}
		mustUnmarshal(data, entry)
		consensus.logger.Infof("[%s] replayWal replay entry:[%d,%d,%d] index:[%d] type: %s",
			consensus.Id, entry.Height, entry.HeightFirstIndex, lastIndex, element.Index(), msgbus.Topic(entry.Topic))
		var payload interface{}
		switch msgbus.Topic(entry.Topic) {
		case msgbus.ProposedBlock:
			block := new(common.Block)
			mustUnmarshal(entry.Data, block)
			payload = block
		case msgbus.VerifyResult:
			result := new(consensuspb.VerifyResult)
			mustUnmarshal(entry.Data, result)
			payload = result
		case msgbus.BlockInfo:
			info := new(common.BlockInfo)
			mustUnmarshal(entry.Data, info)
			payload = info
		case msgbus.RecvConsensusMsg:
			msg := new(netpb.NetMsg)
			mustUnmarshal(entry.Data, msg)
			payload = msg
		}

		msg := &msgbus.Message{
			Topic:   msgbus.Topic(entry.Topic),
			Payload: payload,
		}
		consensus.onMessage(msg, true)
	}
	return nil
}

func (consensus *ConsensusABFTImpl) deleteWalEntry(num uint64) error {
	// Block height is begin from 0,Delete the block data every 10 blocks.
	// If the block height is 10, there are 11 blocks in total and delete the consensus state data of the 11 blocks
	i := num % 10
	if i != 0 {
		return nil
	}
	// attention: due to the feature of lws, the latest file can not be purge
	if err := consensus.wal.Purge(lws.PurgeWithSoftEntries(1), lws.PurgeWithAsync()); err != nil {
		consensus.logger.Errorf("[%s] deleteWalEntry failed, err: %v", consensus.Id, err)
		return err
	}
	consensus.logger.Debugf("[%s] deleteWalEntry success! height: %d, walLastIndex: %d, heightFirstIndex: %d",
		consensus.Id, consensus.height, consensus.lastIndex, consensus.heightFirstIndex)
	return nil
}
