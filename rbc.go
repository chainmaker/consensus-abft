/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package abft

import (
	"bytes"
	"crypto/sha256"
	"fmt"
	"sort"
	"sync"

	"chainmaker.org/chainmaker/pb-go/v3/consensus/abft"
	abftpb "chainmaker.org/chainmaker/pb-go/v3/consensus/abft"
	"github.com/NebulousLabs/merkletree"
	"github.com/klauspost/reedsolomon"
)

// RBC represents an instance of "Reliable Broadcast".
type RBC struct {
	*Config
	sync.Mutex
	enc reedsolomon.Encoder

	messages []*abftpb.ABFTMessageReq // ProofRequest, EchoRequest or ReadyRequest request

	receivedEchos map[string]*abft.EchoRequest // cache EchoRequest msg, key:nodeId, val:EchoRequest
	echoSent      bool                         // echoSent indicates whether has sent ECHO msg

	receivedReadys map[string][]byte /// cache the RootHash in Ready msg, key:nodeId, val:RootHash
	readySent      bool              // readySent indicates whether has sent READY msg

	output        []byte // delivered tx batch
	outputDecoded bool   // outputDecoded indicates whether has decoded tx batch by f+1 data shards
}

// NewRBC returns an instance of RBC for reliable broadcast.
func NewRBC(cfg *Config) *RBC {
	cfg.logger.Infof("NewRBC config: %s", cfg)
	// the number of data shards is f+1, that means only need f+1 echo to decode data,
	// the number of parity shards is N-(f+1),
	// both should not be modified after starting.
	enc, err := reedsolomon.New(cfg.faultsNum+1, cfg.nodesNum-(cfg.faultsNum+1))
	if err != nil {
		cfg.logger.Panicf("[%s] new reedsolomon error: %v", cfg.nodeID, err)
	}

	rbc := &RBC{
		Config:         cfg,
		enc:            enc,
		receivedEchos:  make(map[string]*abft.EchoRequest),
		receivedReadys: make(map[string][]byte),
		messages:       []*abftpb.ABFTMessageReq{},
	}
	return rbc
}

// Input inputs data to the rbc instance.
func (rbc *RBC) Input(data []byte) error {
	rbc.logger.Infof("[%s](%d-%s)(%d) RBC input data.len: %v",
		rbc.nodeID, rbc.height, rbc.id, rbc.height, len(data))
	rbc.Lock()
	defer rbc.Unlock()
	shards, err := rbc.makeShards(data)
	if err != nil {
		return err
	}

	// broadcast ProofRequest(VAL) to nodes
	for i := 0; i < len(shards); i++ {
		tree := merkletree.New(sha256.New())
		if err = tree.SetIndex(uint64(i)); err != nil {
			rbc.logger.Errorf("[%s](%d-%s)(%d) RBC SetIndex failed, err: %v",
				rbc.nodeID, rbc.height, rbc.id, rbc.height, err)
			return err
		}
		for j := 0; j < len(shards); j++ {
			tree.Push(shards[j])
		}

		root, proof, proofIndex, leaves := tree.Prove()
		proofRequest := &abftpb.ProofRequest{
			RootHash: root,
			Proof:    proof,
			Index:    proofIndex,
			Leaves:   leaves,
			Length:   int32(len(data)),
		}
		rbc.appendProofRequests(rbc.nodes[i], proofRequest)
	}

	return nil
}

// HandleMessage handle RBC msg
func (rbc *RBC) HandleMessage(sender string, msg *abftpb.RBCRequest) error {
	rbc.Lock()
	defer rbc.Unlock()

	switch m := msg.Message.(type) {
	case *abft.RBCRequest_ProofRequest:
		return rbc.handleProofRequest(sender, m.ProofRequest)
	case *abft.RBCRequest_EchoRequest:
		return rbc.handleEchoRequest(sender, m.EchoRequest)
	case *abft.RBCRequest_ReadyRequest:
		return rbc.handleReadyRequest(sender, m.ReadyRequest)
	default:
		rbc.logger.Errorf("[%s](%d) RBC receive invalid message: %+v, this should not happen",
			rbc.nodeID, rbc.height, msg)
	}
	return nil
}

// Messages return RBC messages to ACS
func (rbc *RBC) Messages() []*abftpb.ABFTMessageReq {
	rbc.Lock()
	defer rbc.Unlock()

	msgs := rbc.messages
	rbc.messages = []*abftpb.ABFTMessageReq{}
	return msgs
}

// Output output delivered tx batch to ACS
func (rbc *RBC) Output() []byte {
	rbc.Lock()
	defer rbc.Unlock()

	if rbc.output != nil {
		output := rbc.output
		rbc.output = nil
		return output
	}

	return nil
}

// makeShards splits the data to shards with reedsolomon encoder.
func (rbc *RBC) makeShards(data []byte) ([][]byte, error) {
	shards, err := rbc.enc.Split(data)
	if err != nil {
		return nil, err
	}

	if err := rbc.enc.Encode(shards); err != nil {
		return nil, err
	}

	return shards, nil
}

// verifyProofRequest verify whether ProofRequest is valid
func (rbc *RBC) verifyProofRequest(proof *abftpb.ProofRequest) bool {
	result := merkletree.VerifyProof(
		sha256.New(),
		proof.RootHash,
		proof.Proof,
		proof.Index,
		proof.Leaves,
	)

	return result
}

func (rbc *RBC) handleProofRequest(sender string, msg *abftpb.ProofRequest) error {
	if sender != rbc.id {
		return fmt.Errorf("[%s](%d-%s)(%d) RBC receive proof request from error node: %s",
			rbc.nodeID, rbc.height, rbc.id, rbc.height, sender)
	}

	// only send ECHO once in rbc, but it will be retry in msgSender
	if rbc.echoSent {
		rbc.logger.Debugf("[%s](%d-%s)(%d) RBC receive proof: %x from: %v multiple times",
			rbc.nodeID, rbc.height, rbc.id, rbc.height, msg.RootHash, sender)
		return ErrDuplicatedRBCRequest
	}

	if !rbc.verifyProofRequest(msg) {
		return fmt.Errorf("[%s](%d-%s) RBC receive invalid proof request from %s",
			rbc.nodeID, rbc.height, rbc.id, sender)
	}

	rbc.logger.Infof("[%s](%d-%s)(%d) RBC receive proof: %x from: %v",
		rbc.nodeID, rbc.height, rbc.id, rbc.height, msg.RootHash, sender)
	rbc.echoSent = true
	echo := &abftpb.EchoRequest{ProofRequest: msg}
	rbc.appendEchoRequests(echo)
	return nil
}

func (rbc *RBC) handleEchoRequest(sender string, msg *abftpb.EchoRequest) error {
	if _, ok := rbc.receivedEchos[sender]; ok {
		rbc.logger.Debugf("[%s](%d) receive multiple echos from: %s", rbc.nodeID, rbc.height, sender)
		return ErrDuplicatedRBCRequest
	}

	if !rbc.verifyProofRequest(msg.ProofRequest) {
		return fmt.Errorf("[%s] receive invalid proof request from %s", rbc.nodeID, sender)
	}

	rbc.receivedEchos[sender] = msg
	rbc.logger.Infof("[%s](%d-%s)(%d) RBC receive echo: %x from: %v, echoCount: %v, readyCount: %v",
		rbc.nodeID, rbc.height, rbc.id, rbc.height, msg.ProofRequest.RootHash, sender,
		rbc.countEchos(msg.ProofRequest.RootHash), rbc.countReady(msg.ProofRequest.RootHash))

	// only send READY once in rbc when it has received more than N-f ECHO, but it will be retry in msgSender
	// may be in ECHO phrase, rbc has received at least f+1 ECHO and N-f READY, then try decode value
	if rbc.readySent || rbc.countEchos(msg.ProofRequest.RootHash) < rbc.nodesNum-rbc.faultsNum {
		return rbc.tryDecodeValue(msg.ProofRequest.RootHash)
	}

	// when received N-f ECHO, broadcast READY
	rbc.readySent = true
	ready := &abftpb.ReadyRequest{RootHash: msg.ProofRequest.RootHash}
	rbc.appendReadyRequests(ready)
	return nil
}

func (rbc *RBC) handleReadyRequest(sender string, msg *abftpb.ReadyRequest) error {
	if _, ok := rbc.receivedReadys[sender]; ok {
		rbc.logger.Debugf("[%s](%d) receive multiple readys from: %s", rbc.nodeID, rbc.height, sender)
		return ErrDuplicatedRBCRequest
	}
	rbc.receivedReadys[sender] = msg.RootHash
	rbc.logger.Infof("[%s](%d-%s)(%d) RBC receive ready: %x from: %v, echoCount: %v, readyCount: %v",
		rbc.nodeID, rbc.height, rbc.id, rbc.height, msg.RootHash, sender,
		rbc.countEchos(msg.RootHash), rbc.countReady(msg.RootHash))

	// upon receiving f+1 matching READY(h) messages and if READY has not yet been sent, multicast READY(h)
	// may be no received VAL or ECHO now, but it will receive all message finally.
	if rbc.countReady(msg.RootHash) == rbc.faultsNum+1 && !rbc.readySent {
		rbc.readySent = true
		ready := &abftpb.ReadyRequest{RootHash: msg.RootHash}
		rbc.appendReadyRequests(ready)
	}
	return rbc.tryDecodeValue(msg.RootHash)
}

func (rbc *RBC) countEchos(hash []byte) int {
	n := 0
	for _, e := range rbc.receivedEchos {
		if bytes.Equal(hash, e.ProofRequest.RootHash) {
			n++
		}
	}
	return n
}

func (rbc *RBC) countReady(hash []byte) int {
	n := 0
	for _, r := range rbc.receivedReadys {
		if bytes.Equal(hash, r) {
			n++
		}
	}
	return n
}

func (rbc *RBC) tryDecodeValue(hash []byte) error {
	rbc.logger.Debugf("[%s](%d-%s)(%d) RBC tryDecodeValue outputDecoded: %v, countEchos: %v, countReady: %v",
		rbc.nodeID, rbc.height, rbc.id, rbc.height, rbc.outputDecoded, rbc.countEchos(hash), rbc.countReady(hash))

	// attention:
	// if ECHO msg is f+1, then the rbc should receive N-f READY msg to decode
	// if ECHO msg is N-2f, then the rbc only should receive 2f+1 READY msg to decode
	// if decoding by f+1 ECHO msg and 2f+1 READY msg, it will be attacked by the Byzantines
	// f N   f+1  N-2f   2f+1  N-f
	// 1 5    2    3      3     4
	// 1 6    2    4      3     5

	// if rbc instance has delivered(only decode once)
	// or condition for decoding are not met
	// return nil
	if rbc.outputDecoded ||
		rbc.countEchos(hash) < rbc.faultsNum+1 ||
		rbc.countReady(hash) < rbc.nodesNum-rbc.faultsNum {
		return nil
	}

	// if the condition for decoding is met, then decode to output
	var prfs proofs
	for _, echo := range rbc.receivedEchos {
		prfs = append(prfs, echo.ProofRequest)
	}
	sort.Sort(prfs)

	shards := make([][]byte, rbc.nodesNum)
	for _, p := range prfs {
		shards[p.Index] = p.Proof[0]
	}
	if err := rbc.enc.Reconstruct(shards); err != nil {
		rbc.logger.Errorf("[%s](%d-%s)(%d) RBC decode output failed, root: %x, err: %v",
			rbc.nodeID, rbc.height, rbc.id, rbc.height, hash, err)
		return err
	}

	for _, data := range shards[:rbc.faultsNum+1] {
		rbc.output = append(rbc.output, data...)
	}
	rbc.outputDecoded = true
	rbc.output = rbc.output[:prfs[0].Length]
	rbc.logger.Infof("[%s](%d-%s)(%d) RBC decode output data.len: %v, root: %x",
		rbc.nodeID, rbc.height, rbc.id, rbc.height, len(rbc.output), hash)

	return nil
}

func (rbc *RBC) appendProofRequests(to string, proof *abftpb.ProofRequest) {
	rbcRequest := &abftpb.RBCRequest{
		Message: &abftpb.RBCRequest_ProofRequest{
			ProofRequest: proof,
		},
	}
	acsMessage := &abftpb.ACSMessage{
		Message: &abftpb.ACSMessage_Rbc{
			Rbc: rbcRequest,
		},
	}

	abftMessage := &abftpb.ABFTMessageReq{
		Height: rbc.height,
		From:   rbc.nodeID,
		To:     to,
		Id:     rbc.id,
		Acs:    acsMessage,
	}
	rbc.messages = append(rbc.messages, abftMessage)
	rbc.logger.Infof("[%s](%d-%s)(%d) RBC append proof(%x-%v-%v) to: %v",
		rbc.nodeID, rbc.height, rbc.id, rbc.height, proof.RootHash, proof.Index, proof.Leaves, to)
}

func (rbc *RBC) appendEchoRequests(echo *abftpb.EchoRequest) {
	rbcRequest := &abftpb.RBCRequest{
		Message: &abftpb.RBCRequest_EchoRequest{
			EchoRequest: echo,
		},
	}
	acsMessage := &abftpb.ACSMessage{
		Message: &abftpb.ACSMessage_Rbc{
			Rbc: rbcRequest,
		},
	}

	for _, n := range rbc.nodes {
		abftMessage := &abftpb.ABFTMessageReq{
			Height: rbc.height,
			From:   rbc.nodeID,
			To:     n,
			Id:     rbc.id,
			Acs:    acsMessage,
		}
		rbc.messages = append(rbc.messages, abftMessage)
		rbc.logger.Infof("[%s](%d-%s)(%d) RBC append echo id: %v, to: %v",
			rbc.nodeID, rbc.height, rbc.id, rbc.height, rbc.id, n)
	}
}

func (rbc *RBC) appendReadyRequests(ready *abftpb.ReadyRequest) {
	rbcRequest := &abftpb.RBCRequest{
		Message: &abftpb.RBCRequest_ReadyRequest{
			ReadyRequest: ready,
		},
	}
	acsMessage := &abftpb.ACSMessage{
		Message: &abftpb.ACSMessage_Rbc{
			Rbc: rbcRequest,
		},
	}

	for _, n := range rbc.nodes {
		abftMessage := &abftpb.ABFTMessageReq{
			Height: rbc.height,
			From:   rbc.nodeID,
			To:     n,
			Id:     rbc.id,
			Acs:    acsMessage,
		}
		rbc.messages = append(rbc.messages, abftMessage)
		rbc.logger.Infof("[%s](%d-%s)(%d) RBC append ready id: %v, to: %v",
			rbc.nodeID, rbc.height, rbc.id, rbc.height, rbc.id, n)
	}
}

type proofs []*abftpb.ProofRequest

func (p proofs) Len() int           { return len(p) }
func (p proofs) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
func (p proofs) Less(i, j int) bool { return p[i].Index < p[j].Index }

// RBCStateJson is rbc state json
type RBCStateJson struct {
	ReceivedEchos map[string]*abft.EchoRequest `json:"received_echos"`
	EchoSent      bool                         `json:"echo_sent"`

	ReceivedReadys map[string][]byte `json:"received_readys"`
	ReadySent      bool              `json:"ready_sent"`

	Output        []byte `json:"output"`
	OutputDecoded bool   `json:"output_decoded"`
}

// GetRBCStateJson return RBCStateJson
func (rbc *RBC) GetRBCStateJson() *RBCStateJson {
	rbc.Lock()
	defer rbc.Unlock()

	return &RBCStateJson{
		ReceivedEchos:  rbc.receivedEchos,
		EchoSent:       rbc.echoSent,
		ReceivedReadys: rbc.receivedReadys,
		ReadySent:      rbc.readySent,
		Output:         rbc.output,
		OutputDecoded:  rbc.outputDecoded,
	}
}
