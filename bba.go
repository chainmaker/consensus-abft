/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package abft

import (
	"fmt"
	"strings"
	"sync"

	abftpb "chainmaker.org/chainmaker/pb-go/v3/consensus/abft"
)

type bvalDelayedMsg struct {
	sender string
	bval   *abftpb.BValRequest
}

type auxDelayedMsg struct {
	sender string
	aux    *abftpb.AuxRequest
}

type receivedVals struct {
	bba *BBA
	typ string
	set map[string][]bool // key: nodeId, val: []bool
}

func newReceivedVals(bba *BBA, typ string) *receivedVals {
	return &receivedVals{
		bba: bba,
		typ: typ,
		set: make(map[string][]bool),
	}
}

func (r *receivedVals) String() string {
	var builder strings.Builder
	fmt.Fprintf(&builder, "type: %s, set{", r.typ)
	for k, vals := range r.set {
		fmt.Fprintf(&builder, "%s: [", k)
		for _, v := range vals {
			fmt.Fprintf(&builder, "%v, ", v)
		}
		builder.WriteString("], ")
	}
	builder.WriteString("}")
	return builder.String()
}

func (r *receivedVals) addVal(sender string, val bool) error {
	if vals, ok := r.set[sender]; ok {
		for _, v := range vals {
			if v == val {
				r.bba.logger.Debugf("[%s](%d-%s-%d)(%d) BBA %s receivedVals add val: %v multiple times",
					r.bba.nodeID, r.bba.height, r.bba.id, r.bba.epoch, r.bba.height, r.typ, val)
				return ErrDuplicatedRBCRequest
			}
		}
	}

	r.set[sender] = append(r.set[sender], val)
	return nil
}

func (r *receivedVals) countVals(val bool) int {
	n := 0
	for _, s := range r.set {
		for _, v := range s {
			if v == val {
				n++
			}
		}
	}
	return n
}

func (r *receivedVals) length() int {
	return len(r.set)
}

// BBA is asynchronous binary byzantine agreement instance
type BBA struct {
	*Config
	sync.Mutex

	epoch uint32

	messages []*abftpb.ABFTMessageReq // BVAL or AUX request

	estimated  bool // estimated indicates whether set est in this epoch
	estimation bool // estimation is the est value in this epoch

	binValues []bool // cache the b after receive 2f+1 coincident BVAL msg in this epoch
	sentBvals []bool // cache the BVAL msg sent by bba in this epoch

	receivedBvals *receivedVals // key:nodeId, val:[]bool, cache BVAL msg from different node in this epoch
	receivedAux   *receivedVals // key:nodeId, val:[]bool, cache AUX msg from different node in this epoch

	outputted bool // outputted indicates whether has delivered output to acs, only deliver once
	output    bool // output is the value delivered to acs, after delivered, it set to false

	decided  bool // decided indicates whether this bba instance is finished
	decision bool // decision is the value delivered to acs

	done bool // done indicates whether this bba instance is finished, if finished, bba msg is no longer processed

	bvalBuffer []bvalDelayedMsg // cache the higher epoch BVAL msg
	auxBuffer  []auxDelayedMsg  // cache the higher epoch AUX msg
}

// NewBBA create BBA
func NewBBA(cfg *Config) *BBA {
	cfg.logger.Infof("NewBBA config: %s", cfg)
	bba := &BBA{
		Config:    cfg,
		epoch:     0,
		binValues: []bool{},
		sentBvals: []bool{},
	}
	bba.receivedBvals = newReceivedVals(bba, "Bvals")
	bba.receivedAux = newReceivedVals(bba, "Aux")

	return bba
}

// Messages deliver messages to ACS
func (bba *BBA) Messages() []*abftpb.ABFTMessageReq {
	bba.Lock()
	defer bba.Unlock()

	messages := bba.messages
	bba.messages = []*abftpb.ABFTMessageReq{}
	return messages
}

// AcceptInput whether can set est
func (bba *BBA) AcceptInput() bool {
	bba.Lock()
	defer bba.Unlock()

	return bba.epoch == 0 && !bba.estimated
}

// Input input val to set est and broadcast BVAL msg
func (bba *BBA) Input(val bool) error {
	if !bba.AcceptInput() {
		return nil
	}

	bba.Lock()
	defer bba.Unlock()
	bba.logger.Infof("[%s](%d-%s-%d)(%d) BBA Input val: %v", bba.nodeID, bba.height, bba.id, bba.epoch, bba.height, val)

	// set est and broadcast BVAL msg
	bba.estimated = true
	bba.estimation = val
	bba.sentBvals = append(bba.sentBvals, val)
	bba.appendBValRequests(val)
	return nil
}

// HandleMessage handle BVAL and AUX msg
func (bba *BBA) HandleMessage(sender string, msg *abftpb.BBARequest) error {
	bba.Lock()
	defer bba.Unlock()

	bba.logger.Debugf("[%s](%d-%s-%d)(%d) BBA HandleMessage from: %v",
		bba.nodeID, bba.height, bba.id, bba.epoch, bba.height, sender)
	if bba.done {
		return nil
	}
	switch m := msg.Message.(type) {
	case *abftpb.BBARequest_Bval:
		return bba.handleBvalRequest(sender, m.Bval)
	case *abftpb.BBARequest_Aux:
		return bba.handleAuxRequest(sender, m.Aux)
	default:
		bba.logger.Errorf("[%s](%d-%s-%s)(%d) BBA receive invalid message: %+v, this should not happen",
			bba.nodeID, bba.height, bba.id, bba.epoch, bba.height, msg)
	}
	return nil
}

// Output deliver bba result to ACS only once
func (bba *BBA) Output() (outputted bool, output bool) {
	bba.Lock()
	defer bba.Unlock()

	outputted = bba.outputted
	output = bba.output
	bba.logger.Debugf("[%s](%d-%s-%d)(%d) BBA outputted: %v, output: %v",
		bba.nodeID, bba.height, bba.id, bba.epoch, bba.height, outputted, output)

	bba.outputted = false
	bba.output = false

	return outputted, output
}

func (bba *BBA) handleBvalRequest(sender string, bval *abftpb.BValRequest) error {
	// abft will notify other nodes no send msg again
	if bval.Epoch < bba.epoch {
		bba.logger.Debugf("[%s](%d-%s-%d)(%d) BBA receive outdated Bval from: %v",
			bba.nodeID, bba.height, bba.id, bba.epoch, bba.height, sender)
		return nil
	}

	// cache higher epoch msg
	if bval.Epoch > bba.epoch {
		bba.bvalBuffer = append(bba.bvalBuffer, bvalDelayedMsg{sender: sender, bval: bval})
		return nil
	}

	val := bval.Value

	if err := bba.receivedBvals.addVal(sender, val); err != nil {
		return err
	}
	bvalCount := bba.receivedBvals.countVals(val)

	bba.logger.Infof("[%s](%d-%s-%d)(%d) BBA receive Bval value: %v, from: %v, bvalCount: %v, auxCount: %v",
		bba.nodeID, bba.height, bba.id, bba.epoch, bba.height, bval.Value, sender, bvalCount, bba.receivedAux.countVals(val))

	// if bba instance has received f+1 b and no sent BVAL(b) in this epoch
	// then send this b as BVAL to other nodes
	// attention: a node may send BVAL(0) and BVAL(1) in an epoch
	if bvalCount == bba.faultsNum+1 && !bba.hasSentBval(val) {
		bba.sentBvals = append(bba.sentBvals, val)
		bba.appendBValRequests(val)
	}

	// send AUX request when received 2f+1 BVAL(b) with consistent value in this epoch
	// attention: a node may send BVAL(0) and BVAL(1) in an epoch
	if bvalCount == 2*bba.faultsNum+1 {
		for _, v := range bba.binValues {
			// Exits if the bba.binValues set contains val already
			if v == val {
				return nil
			}
		}
		bba.binValues = append(bba.binValues, val)
		bba.logger.Infof("[%s](%d-%s-%d)(%d) BBA handleBvalRequest binValues: %v",
			bba.nodeID, bba.height, bba.id, bba.epoch, bba.height, bba.binValues)

		bba.appendAuxRequests(val)
		return nil
	}
	return nil
}

func (bba *BBA) handleAuxRequest(sender string, aux *abftpb.AuxRequest) error {
	// abft will notify other nodes no send msg again
	if aux.Epoch < bba.epoch {
		bba.logger.Debugf("[%s](%d-%s-%d)(%d) BBA receive outdated aux from: %v",
			bba.nodeID, bba.height, bba.id, bba.epoch, bba.height, sender)
		return nil
	}

	// cache higher epoch msg
	if aux.Epoch > bba.epoch {
		bba.auxBuffer = append(bba.auxBuffer, auxDelayedMsg{sender: sender, aux: aux})
		return nil
	}

	// add val to receivedAux
	val := aux.Value
	if err := bba.receivedAux.addVal(sender, val); err != nil {
		return err
	}

	bba.logger.Infof("[%s](%d-%s-%d)(%d) BBA receive Aux value: %v from: %v, bvalCount: %v, auxCount: %v",
		bba.nodeID, bba.height, bba.id, bba.epoch, bba.height, aux.Value, sender,
		bba.receivedBvals.countVals(aux.Value), bba.receivedAux.countVals(aux.Value))

	// if bba instance has received AUX from N-f nodes
	// try decide or finish to a value
	bba.tryOutputAgreement()
	return nil
}

func (bba *BBA) tryOutputAgreement() {
	// binValues should not be nil
	if len(bba.binValues) == 0 {
		return
	}
	// judge whether bba instance has received AUX from N-f nodes
	lenOutputs, vals := bba.countOutputs()
	if lenOutputs < bba.nodesNum-bba.faultsNum {
		return
	}

	// a bba instance finish/done condition is that:
	// (1) receive N-f AUX in this epoch
	// (2) it has decided in a value b in previous epoch and the decided value b equals to the coin in this epoch
	// attention: the decided value in previous epoch may be changed

	coin := bba.epoch%2 == 0
	if bba.done || (bba.decided && bba.decision == coin) {
		bba.logger.Infof("[%s](%d-%s-%d)(%d) BBA finishAgreement val: %v, coin: %v",
			bba.nodeID, bba.height, bba.id, bba.epoch, bba.height, bba.decision, coin)
		bba.done = true
		return
	}

	bba.logger.Debugf("[%s](%d-%s-%d)(%d) BBA tryOutputAgreement vals: %v, coin: %v",
		bba.nodeID, bba.height, bba.id, bba.epoch, bba.height, vals, coin)

	// a bba instance decide/output condition is that:
	// (1) receive at least N-f AUX in this epoch
	// (2) only has one value b in binValues in this epoch
	// (3) b == coin in this epoch

	if len(vals) == 1 {
		bba.estimation = vals[0]
		if !bba.decided && vals[0] == coin {
			bba.decided = true
			bba.decision = vals[0]
			bba.outputted = true
			bba.output = vals[0]
			bba.logger.Infof("[%s](%d-%s-%d)(%d) BBA outputAgreement val: %v, coin: %v",
				bba.nodeID, bba.height, bba.id, bba.epoch, bba.height, vals[0], coin)
		}
	} else {
		bba.estimation = coin
	}

	bba.increaseEpoch()
	bba.sentBvals = append(bba.sentBvals, bba.estimation)
	bba.appendBValRequests(bba.estimation)

	// process BVAL in new epoch
	bvalBuffer := bba.bvalBuffer
	bba.bvalBuffer = nil
	for _, msg := range bvalBuffer {
		err := bba.handleBvalRequest(msg.sender, msg.bval)
		if err != nil {
			bba.logger.Errorf("[%s](%d-%s-%d)(%d) BBA handleBvalRequest error: %v",
				bba.nodeID, bba.height, bba.id, bba.epoch, bba.height, err)
		}
	}

	// process AUX in new epoch
	auxBuffer := bba.auxBuffer
	bba.auxBuffer = nil
	for _, msg := range auxBuffer {
		err := bba.handleAuxRequest(msg.sender, msg.aux)
		if err != nil {
			bba.logger.Errorf("[%s](%d-%s-%d)(%d) BBA handleAuxRequest error: %v",
				bba.nodeID, bba.height, bba.id, bba.epoch, bba.height, err)
		}
	}
}

func (bba *BBA) increaseEpoch() {
	bba.binValues = []bool{}
	bba.sentBvals = []bool{}
	bba.receivedBvals = newReceivedVals(bba, "Bvals")
	bba.receivedAux = newReceivedVals(bba, "Aux")
	bba.epoch++
	bba.logger.Infof("[%s](%d-%s-%d)(%d) BBA increaseEpoch", bba.nodeID, bba.height, bba.id, bba.epoch, bba.height)
}

func (bba *BBA) countOutputs() (int, []bool) {
	length := bba.receivedAux.length()

	bba.logger.Debugf("[%s](%d-%s-%d)(%d) BBA countOutputs receivedAux: %s, binValues: %v",
		bba.nodeID, bba.height, bba.id, bba.epoch, bba.height, bba.receivedAux.String(), bba.binValues)
	return length, bba.binValues
}

func (bba *BBA) hasSentBval(val bool) bool {
	for _, ok := range bba.sentBvals {
		if ok == val {
			return true
		}
	}
	return false
}

func (bba *BBA) appendBValRequests(val bool) {
	bvalRequest := &abftpb.BValRequest{
		Epoch: bba.epoch,
		Value: val,
	}
	bbaRequest := &abftpb.BBARequest{
		Message: &abftpb.BBARequest_Bval{
			Bval: bvalRequest,
		},
	}
	acsMessage := &abftpb.ACSMessage{
		Message: &abftpb.ACSMessage_Bba{
			Bba: bbaRequest,
		},
	}

	for _, n := range bba.nodes {
		abftMessage := &abftpb.ABFTMessageReq{
			Height: bba.height,
			From:   bba.nodeID,
			To:     n,
			Id:     bba.id,
			Acs:    acsMessage,
		}
		bba.messages = append(bba.messages, abftMessage)
		bba.logger.Infof("[%s](%d-%s-%d)(%d) BBA appendBValRequests value: %v to: %v",
			bba.nodeID, bba.height, bba.id, bba.epoch, bba.height, val, n)
	}
}

func (bba *BBA) appendAuxRequests(val bool) {
	auxRequest := &abftpb.AuxRequest{
		Epoch: bba.epoch,
		Value: val,
	}
	bbaRequest := &abftpb.BBARequest{
		Message: &abftpb.BBARequest_Aux{
			Aux: auxRequest,
		},
	}
	acsMessage := &abftpb.ACSMessage{
		Message: &abftpb.ACSMessage_Bba{
			Bba: bbaRequest,
		},
	}

	for _, n := range bba.nodes {
		abftMessage := &abftpb.ABFTMessageReq{
			Height: bba.height,
			From:   bba.nodeID,
			To:     n,
			Id:     bba.id,
			Acs:    acsMessage,
		}
		bba.messages = append(bba.messages, abftMessage)
		bba.logger.Infof("[%s](%d-%s-%d)(%d) BBA appendAuxRequests value: %v to: %v",
			bba.nodeID, bba.height, bba.id, bba.epoch, bba.height, val, n)
	}
}

// BBAStateJson is bba state json
type BBAStateJson struct {
	Estimated  bool `json:"estimated"`
	Estimation bool `json:"estimation"`

	BinValues []bool `json:"bin_values"`
	SentBvals []bool `json:"sent_bvals"`

	Outputted bool `json:"outputted"`
	Output    bool `json:"output"`

	Decided  bool `json:"decided"`
	Decision bool `json:"decision"`

	Done bool `json:"done"`
}

// GetBBAStateJson return BBAStateJson
func (bba *BBA) GetBBAStateJson() *BBAStateJson {
	bba.Lock()
	defer bba.Unlock()

	return &BBAStateJson{
		Estimated:  bba.estimated,
		Estimation: bba.estimation,

		BinValues: bba.binValues,
		SentBvals: bba.sentBvals,

		Outputted: bba.outputted,
		Output:    bba.output,

		Decided:  bba.decided,
		Decision: bba.decision,

		Done: bba.done,
	}
}
