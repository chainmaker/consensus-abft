module chainmaker.org/chainmaker/consensus-abft/v3

go 1.16

require (
	chainmaker.org/chainmaker/common/v3 v3.0.0
	chainmaker.org/chainmaker/consensus-utils/v3 v3.0.0
	chainmaker.org/chainmaker/localconf/v3 v3.0.0
	chainmaker.org/chainmaker/logger/v3 v3.0.0
	chainmaker.org/chainmaker/lws v1.2.0
	chainmaker.org/chainmaker/pb-go/v3 v3.0.0
	chainmaker.org/chainmaker/protocol/v3 v3.0.0
	chainmaker.org/chainmaker/utils/v3 v3.0.0
	github.com/NebulousLabs/errors v0.0.0-20181203160057-9f787ce8f69e // indirect
	github.com/NebulousLabs/fastrand v0.0.0-20181203155948-6fb6489aac4e // indirect
	github.com/NebulousLabs/merkletree v0.0.0-20181203152040-08d5d54b07f5
	github.com/gogf/gf v1.16.4
	github.com/gogo/protobuf v1.3.2
	github.com/golang/mock v1.6.0
	github.com/klauspost/reedsolomon v1.9.12
	github.com/stretchr/testify v1.8.0
	github.com/test-go/testify v1.1.4
	github.com/thoas/go-funk v0.8.0
)

replace (
	github.com/libp2p/go-libp2p-core => chainmaker.org/chainmaker/libp2p-core v1.0.0
	github.com/lucas-clemente/quic-go v0.26.0 => chainmaker.org/third_party/quic-go v1.0.0
	github.com/marten-seemann/qtls-go1-15 => chainmaker.org/third_party/qtls-go1-15 v1.0.0
	github.com/marten-seemann/qtls-go1-16 => chainmaker.org/third_party/qtls-go1-16 v1.0.0
	github.com/marten-seemann/qtls-go1-17 => chainmaker.org/third_party/qtls-go1-17 v1.0.0
	github.com/marten-seemann/qtls-go1-18 => chainmaker.org/third_party/qtls-go1-18 v1.0.0
)
